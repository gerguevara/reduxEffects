import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as usuariosActions from '../actions';
import { tap, mergeMap, map, catchError } from 'rxjs/operators';
import { UsuarioService } from '../../services/usuario.service';
import { of } from 'rxjs';



@Injectable()
export class UsuarioEffects {

  constructor(
    private actions$: Actions,
    private usuarioService: UsuarioService
  ) { }

  // creandoo un efecto
  cargarUsuario$ = createEffect(
    //primero necesita un callback
    // se pasa por el pipe porque de lo contrario esucharia a todos las acciones, y solo queremos escuhar una
    //ofType es un operador propio de ngrx
    () => this.actions$.pipe(
      ofType(usuariosActions.cargarUsuario),
      /*mergaMap- ayuda a dispara un nuevo observable y
      coombinarlo con el anterior, en este caso se usara usuario service*/
      mergeMap(
        // al escuchar las acciones, los efectos efectos pueden tomarlas y usar sus propiedades
        (action) => this.usuarioService.getUserById(action.id)
          .pipe(
            map(user => usuariosActions.cargarUsuarioSuccess({ usuario: user })),
                 // catch error captura el error http , pero no es un observable,
            catchError(err => of(usuariosActions.cargarUsuarioError({ payload: err })))
          )
      )
    )
  );

}
