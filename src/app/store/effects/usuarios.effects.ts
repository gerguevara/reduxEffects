import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { of } from "rxjs";
import { catchError, map, mergeMap, tap } from "rxjs/operators";
import { UsuarioService } from "src/app/services/usuario.service";
import * as usuariosActions from "../actions/usuarios.actions";



@Injectable()
export class UsuariosEffects {

  // el actios es un simple observable que escucha las acciones
  constructor(private actions$: Actions, private usuarioService: UsuarioService) { }

  // creandoo un efecto
  cargarUsuarios$ = createEffect(
    //primero necesita un callback
    // se pasa por el pipe p[orque de lo contrario esucharia a todos las acciones, y solo queremos escuhar una
    //ofType es un operador propio de ngrx
    () => this.actions$.pipe(
      ofType(usuariosActions.cargarUsuarios),
      /*mergaMap- ayuda a dispara un nuevo observable y
      coombinarlo con el anterior, en este caso se usara usuario service*/
      mergeMap(
        () => this.usuarioService.getUsers()
          .pipe(
            map(users => usuariosActions.cargarUsuariosSuccess({ usuarios: users })),
            // catch error captura el error http , pero no es un observable,
            //por eso se utiliza el of para  transforma esa respues en observable
            catchError(error => of (usuariosActions.cargarUsuariosError({payload: error})))
          )

      )
    )

  )
}

