import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducers';
import { cargarUsuario } from '../../store/actions/usuario.actions';
import { Usuario } from '../../models/usuario.model';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styles: []
})
export class UsuarioComponent implements OnInit {

  usuario: Usuario | any;

  constructor( private router: ActivatedRoute,
               private store: Store<AppState>) { }

  ngOnInit() {

    // toma el usuario  de store para renderizasarlo
    this.store.select('usuario').subscribe( ({ user}) => {
      this.usuario = user;
    });

    // toma el p[arametro de la url y lo usa para disparar la accion  que es escuchada por el efecto

    this.router.params.subscribe( ({ id }) => {

      this.store.dispatch( cargarUsuario({ id }) );

    });

  }

}
